# -*- coding: utf-8 -*-
from odoo import api, exceptions, fields, models, tools
from odoo.tools.translate import _


class MailMessage(models.Model):
    _inherit = 'mail.message'

    gitlab_id = fields.Integer('GitLab ID')
