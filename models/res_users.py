# -*- coding: utf-8 -*-
from odoo import api, exceptions, fields, models, tools
from odoo.tools.translate import _


class ResUsers(models.Model):
    _inherit = 'res.users'

    gitlab_username = fields.Char('GitLab Username')
    gitlab_token = fields.Char('GitLab Token')
    gitlab_id = fields.Integer('GitLab ID')
