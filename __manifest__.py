# -*- coding: utf-8 -*-
{
    'name': 'Odoo Gitlab Integration',
    'version': '10.1.1',
    'author': 'Mayank Patel',
    'category': 'Server Tools',
    'depends': [
        'web', 'project', 'base', 'hr_timesheet'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/users_view.xml',
        'views/webhook_views.xml',
        'views/project_view.xml',
        'views/gitlab_group_views.xml',
    ],
    'auto_install': False,
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
